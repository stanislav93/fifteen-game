import {TestBed, inject} from '@angular/core/testing';

import {VkoauthService} from './vkoauth.service';

describe('VkoauthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VkoauthService]
    });
  });

  it('should be created', inject([VkoauthService], (service: VkoauthService) => {
    expect(service).toBeTruthy();
  }));
});
