import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {VkoauthService} from '../../vkoauth.service';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {Friend} from '../../interface/friend';
import {Location} from '@angular/common';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [VkoauthService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit {
  friends$: Observable<Friend[]>;
  user$: Observable<Friend>;

  constructor(
    public vk: VkoauthService,
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.initCode();
  }

  initCode() {
    const token = this.route.snapshot.queryParams.token;
    if (token !== undefined) {
      this.location.replaceState('');
      this.vk.token = token;
    }
    if (this.vk.token) {
      this.friends$ = this.vk.getFriend(this.vk.token);
      this.user$ = this.vk.getUsers(this.vk.token);
    }
  }
}
