import {Component, Input, OnInit} from '@angular/core';
import {Friend} from '../../interface/friend';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.scss']
})
export class FriendComponent implements OnInit {
  @Input() frined: Friend;

  constructor() {
  }

  ngOnInit() {
  }

}
