import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './components/main/main.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FriendComponent} from './components/friend/friend.component';
import {UserComponent} from './components/user/user.component';

const routes: Routes = [
  {path: '', component: MainComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
  ],
  declarations: [MainComponent, FriendComponent, UserComponent],
})
export class VkoauthModule {
}
