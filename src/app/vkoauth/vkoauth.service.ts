import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Friend} from './interface/friend';
import {map} from 'rxjs/operators';

@Injectable()
export class VkoauthService {
  private readonly backend = 'https://node-fiftin-game1.appspot.com/';
  private readonly liveToken = 84400 * 1000;

  constructor(private http: HttpClient) {
  }

  public getCode(): void {
    window.location.href = `https://oauth.vk.com/authorize?client_id=6758111%20&display=page&redirect_uri=${this.backend}&scope=friends`;
  }

  set token(token: string) {
    const d = new Date();
    d.setTime(d.getTime() + this.liveToken);
    document.cookie = `token=${token};expires=${ d.toUTCString()};path=/`;
  }

  get token() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, '$1');
  }

  public getFriend(token: string): Observable<Friend[]> {
    return this.http.get<Friend[]>(`${this.backend}fiend?token=${token}`);
  }

  public getUsers(token: string): Observable<Friend> {
    return this.http.get<Friend>(`${this.backend}user?token=${token}`);
  }
}
