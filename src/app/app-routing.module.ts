import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: 'game-fifteen', loadChildren: './fifteen/fifteen.module#FifteenModule'},
  {path: '', loadChildren: './vkoauth/vkoauth.module#VkoauthModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
