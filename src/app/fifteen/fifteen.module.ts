import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FifteenMainComponent } from './components/fifteen-main/fifteen-main.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {ItemTrComponent} from './components/item-tr/item-tr.component';
import {FifteenService} from './fifteen.service';
const routes: Routes = [
  {path: '', component: FifteenMainComponent},
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [FifteenMainComponent, ItemTrComponent],
  // providers: [FifteenService]
})
export class FifteenModule { }
