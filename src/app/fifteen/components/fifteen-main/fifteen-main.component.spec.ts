import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FifteenMainComponent } from './fifteen-main.component';

describe('FifteenMainComponent', () => {
  let component: FifteenMainComponent;
  let fixture: ComponentFixture<FifteenMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FifteenMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FifteenMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
