import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {FifteenService} from '../../fifteen.service';

@Component({
  selector: 'app-fifteen-main',
  templateUrl: './fifteen-main.component.html',
  styleUrls: ['./fifteen-main.component.scss'],
  providers: [FifteenService]
})
export class FifteenMainComponent implements OnInit {
  defaulSize = 4;
  defaulStep = 5;

  constructor(public fs: FifteenService) {
  }

  ngOnInit() {
    this.fs.gen({size: this.defaulSize, step: this.defaulStep});
  }

  get items() {
    return this.fs.getData();
  }


  start(f: NgForm): void {
    this.fs.gen(f.form.value);
  }
}
