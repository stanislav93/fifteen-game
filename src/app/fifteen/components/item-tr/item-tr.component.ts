import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FifteenService} from '../../fifteen.service';

@Component({
  selector: 'app-item-tr',
  templateUrl: './item-tr.component.html',
  styleUrls: ['./item-tr.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemTrComponent implements OnInit {
  @Input() tr;
  @Input() biggest;

  constructor(private fs: FifteenService) {

  }

  ngOnInit() {
  }

  numberClick(item: number): void {
    this.fs.changeData(item);
  }
}
