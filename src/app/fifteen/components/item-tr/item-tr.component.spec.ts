import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ItemTrComponent} from './item-tr.component';

describe('ItemTrComponent', () => {
  let component: ItemTrComponent;
  let fixture: ComponentFixture<ItemTrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemTrComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
