import {Injectable} from '@angular/core';

/*{
  providedIn: 'root'
}*/
@Injectable()
export class FifteenService {
  /** Массив вложенных массивов чисел игры */
  private items = [];
  /** На сколько перемешать */
  private step;
  /** Полный размер */
  private size;
  /** Размер части */
  private sizePart;
  /** Число пустого блока для перестраения */
  private biggestElement = 0;

  constructor() {
  }

  public gen(params): void {
    /** Присвоение параметров */
    this.size = params.size ** 2;
    this.sizePart = +params.size;
    this.step = +params.step;
    this.generate();
    const res = {};
    res['biggestIndex'] = this.items.length - 1;
    res['biggestPosition'] = this.items[res['biggestIndex']].findIndex(el => this.biggestElement === el);
    this.depth(res);
  }

  get getBiggest(): number {
    return this.biggestElement;
  }

  public getData(): number[] {
    return this.items;
  }

  /** Смена поля при клике на кнопку */
  public changeData(clickedNumber): void {
    const res = this.findIndex(clickedNumber);
    if ((
      res['changeIndex'] === res['biggestIndex'] && Math.abs(res['changePosition'] - res['biggestPosition']) === 1
      ||
      res['changePosition'] === res['biggestPosition'] && Math.abs(res['changeIndex'] - res['biggestIndex']) === 1
    )) {
      this.items[res['biggestIndex']][res['biggestPosition']] = this.items[res['changeIndex']][res['changePosition']];
      this.items[res['changeIndex']][res['changePosition']] = this.biggestElement;
    }
  }

  /** генерирование поля */
  private generate(): void {
    /** Обнуление итомов что бы они в массиве был правильный стак */
    this.items = [];
    this.biggestElement = 0;
    for (let i = 1; i < this.size; i += this.sizePart) {
      this.senRow(i);
    }
  }

  /** Перемешивание */
  private depth(res): void {
    /** Проверка на то нужн ли вообще что то передвигать */
    if (this.step === 0) {
      return;
    }
    // TODO на этом этапе не понимаю как запутать поле


    if (--this.step) {
      this.depth(res);
    }
  }

  /** Нахождение нужных индексов */
  private findIndex(clickedNumber = -1): object {
    const res = {};
    for (const i of Object.keys(this.items)) {
      for (const e of Object.keys(this.items[i])) {
        if (this.items[i][e] === clickedNumber) {
          res['changeIndex'] = i;
          res['changePosition'] = e;
        }
        if (this.items[i][e] === this.biggestElement) {
          res['biggestIndex'] = i;
          res['biggestPosition'] = e;
        }
      }
    }
    return res;
  }

  private senRow(param): void {
    const arr = [];
    for (let i = param; i < param + this.sizePart; i++) {
      if (i > this.biggestElement) {
        this.biggestElement = i;
      }
      arr.push(i);
    }
    this.items.push(arr);
  }
}
