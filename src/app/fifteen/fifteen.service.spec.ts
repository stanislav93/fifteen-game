import {TestBed, inject} from '@angular/core/testing';

import {FifteenService} from './fifteen.service';

describe('FifteenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FifteenService]
    });
  });

  it('should be created', inject([FifteenService], (service: FifteenService) => {
    expect(service).toBeTruthy();
  }));
});
